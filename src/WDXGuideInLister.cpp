#include <windows.h>
#include <stdio.h>
#include "listplug.h"

#define WDXGUIDEINLISTER "WDXGuideInLister"
#define WDXGUIDEINLISTERW L"WDXGuideInLister"
#define wdirtypemax 1024
#define longnameprefixmax 6
#define _detectstring "MULTIMEDIA"

#ifdef _WIN64
#define PATH "Path64"
#else
#define PATH "Path"
#endif

#ifndef countof
#define countof(str) (sizeof(str)/sizeof(str[0]))
#endif // countof

char* strlcpy(char* p1, char* p2, size_t maxSize);
wchar_t* wcslcpy(wchar_t* str1, const wchar_t* str2, int maxSize);
wchar_t* wcslcat(wchar_t* str1, const wchar_t* str2, int maxSize);
char* walcopy(char* outname, const wchar_t* inname, int maxSize);
wchar_t* awlcopy(wchar_t* outname, const char* inname, int maxSize);
BOOL MakeExtraLongNameW(wchar_t* outbuf, const wchar_t* inbuf, int maxSize);
void DisplayLastErrorMsgW(const wchar_t* lpFunction, const wchar_t* lpFileName, UINT msgType);
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
BOOL CALLBACK EnumWindowsProc(HWND hwnd, LPARAM lParam);

#define wafilenamecopy(outname, inname) walcopy(outname, inname, countof(outname))
#define awfilenamecopy(outname, inname) awlcopy(outname, inname, countof(outname))

static HINSTANCE hinst = nullptr;
static char iniFileName[_MAX_PATH] = "";
static char PathEx[_MAX_PATH] = "";

typedef struct
{
	wchar_t fileName[wdirtypemax];
	DWORD dwProcessId;
	HWND hWndProc;
	HWND hWndParent;
	HANDLE hProcess;
} WndVar;

char* strlcpy(char* p1, char* p2, size_t maxSize)
{
	if ((size_t) strlen(p2) > maxSize)
	{
		strncpy(p1, p2, maxSize - 1);
		p1[maxSize - 1] = 0;
	}
	else
	{
		strcpy(p1, p2);
	}
	return p1;
}

char* walcopy(char* outname, const wchar_t* inname, int maxSize)
{
	if (inname)
	{
		WideCharToMultiByte(CP_ACP, 0, inname, -1, outname, maxSize, NULL, NULL);
		outname[maxSize - 1] = 0;
		return outname;
	}
	else
	{
		return nullptr;
	}
}

wchar_t* awlcopy(wchar_t* outname, const char* inname, int maxSize)
{
	if (inname)
	{
		MultiByteToWideChar(CP_ACP, 0, inname, -1, outname, maxSize);
		outname[maxSize - 1] = 0;
		return outname;
	}
	else
	{
		return nullptr;
	}
}

wchar_t* wcslcpy(wchar_t* str1, const wchar_t* str2, int maxSize)
{
	if ((int)wcslen(str2) >= maxSize - 1)
	{
		wcsncpy(str1, str2, maxSize - 1);
		str1[maxSize - 1] = 0;
	}
	else
	{
		wcscpy(str1, str2);
	}
	return str1;
}

wchar_t* wcslcat(wchar_t* str1, const wchar_t* str2, int maxSize)
{
	int l1 = (int)wcslen(str1);
	if ((int)wcslen(str2) >= maxSize - 1 - l1)
	{
		wcsncat(str1, str2, maxSize - 1 - l1);
		str1[maxSize - 1] = 0;
	}
	else
	{
		wcscat(str1, str2);
	}
	return str1;
}

BOOL MakeExtraLongNameW(wchar_t* outbuf, const wchar_t* inbuf, int maxSize)
{
	if (wcslen(inbuf) > 259)
	{
		if (inbuf[0] == '\\' && inbuf[1] == '\\')
		{   // UNC-Path! Use \\?\UNC\server\share\subdir\name.ext
			wcslcpy(outbuf, L"\\\\?\\UNC", maxSize);
			wcslcat(outbuf, inbuf + 1, maxSize);
		}
		else
		{
			wcslcpy(outbuf, L"\\\\?\\", maxSize);
			wcslcat(outbuf, inbuf, maxSize);
		}
	}
	else
	{
		wcslcpy(outbuf, inbuf, maxSize);
	}
	return (int)wcslen(inbuf) + 3 <= maxSize;
}

void DisplayLastErrorMsgW(const wchar_t* lpFunction, const wchar_t* lpFileName, UINT msgType)
{
	// retrieve the system error message for the last-error code
	DWORD dw = GetLastError();
	if (dw)
	{
		LPVOID lpMsgBuf;
		FormatMessageW(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, dw, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (wchar_t*) &lpMsgBuf, 0, NULL );
		// display the error message
		size_t s = wcslen((const wchar_t*)lpMsgBuf) + wcslen((const wchar_t*)lpFunction) + 40;
		LPVOID lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, (s + 1)*sizeof(wchar_t));
		_snwprintf((wchar_t*)lpDisplayBuf, s, L"%s failed with error %d: %s", lpFunction, dw, lpMsgBuf);
		wchar_t wbuf[wdirtypemax + longnameprefixmax];
		if (MakeExtraLongNameW(wbuf, lpFileName, wdirtypemax + longnameprefixmax))
		{
			MessageBoxW(nullptr, (const wchar_t*)lpDisplayBuf, (const wchar_t*)wbuf, msgType);
		}
		LocalFree(lpMsgBuf);
		LocalFree(lpDisplayBuf);
	}
}

BOOL CALLBACK EnumWindowsProc(HWND hWnd, LPARAM lParam)
{
	WndVar* hWndVar = (WndVar*)lParam;
	if (hWndVar)
	{
		DWORD dwPID;
		GetWindowThreadProcessId(hWnd, &dwPID);

		if (dwPID == hWndVar->dwProcessId)
		{
			char buf[_MAX_PATH];
			GetWindowTextA(hWnd, buf, _MAX_PATH);
			//DWORD dwStyle = GetWindowLong(hWnd, GWL_STYLE);
			//if (dwStyle & WS_OVERLAPPEDWINDOW)
			//if (strlen(buf) > 0 && GetParent(hWnd) == nullptr)
			if (strlen(buf) > 0 && strcmp(buf, "Field value") != 0)
			{
				// dirty hack: do not stop on the "Field value" window
				//MessageBoxA(nullptr, buf, buf, MB_OK);
				hWndVar->hWndProc = hWnd;
				return FALSE;
			}
		}
	}

	return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_CREATE:
		{
			BOOL bError = FALSE;
			SetWindowLongW(hWnd, GWLP_USERDATA, (LONG)((CREATESTRUCT*)lParam)->lpCreateParams);
			WndVar* hWndVar = (WndVar*)((CREATESTRUCT*)lParam)->lpCreateParams;
			if (hWndVar)
			{
				wchar_t* cmdLine = new wchar_t[2*wdirtypemax];
				if (cmdLine)
				{
					WIN32_FIND_DATAA findData = {0};
					SecureZeroMemory(&findData, sizeof(WIN32_FIND_DATAA));
					if (FindFirstFileA(PathEx, &findData) == INVALID_HANDLE_VALUE)
					{
						if (IDYES == MessageBoxW(nullptr, L"Could not find WDX Guide.\n\nDo you want to configure it?", WDXGUIDEINLISTERW, MB_YESNO | MB_ICONWARNING))
						{
							char cmdName[_MAX_PATH];
							OPENFILENAMEA of;
							SecureZeroMemory(&of, sizeof(of));
							of.lStructSize = sizeof(of);
							of.hwndOwner = nullptr;
							of.lpstrFilter = "Executable Files\0*.exe\0";
							of.nFilterIndex = 1;
							of.lpstrFile = cmdName;
							of.lpstrFile[0] = '\0';
							of.nMaxFile = _MAX_PATH - 1;
							of.lpstrFileTitle = nullptr;
							of.nMaxFileTitle = 0;
							of.lpstrInitialDir = nullptr;
							of.lpstrTitle = "Choose WDX Guide";
							of.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY;

							if (GetOpenFileNameA(&of))
							{
								WritePrivateProfileStringA(WDXGUIDEINLISTER, PATH, of.lpstrFile, iniFileName);
								strlcpy(PathEx, of.lpstrFile, _MAX_PATH);
							}
						}
					}
					wchar_t PathExW[wdirtypemax];
					awfilenamecopy(PathExW, PathEx);

					wcslcpy(cmdLine, L"\"", 2*wdirtypemax);
					wcslcat(cmdLine, PathExW, 2*wdirtypemax);
					wcslcat(cmdLine, L"\" \"", 2*wdirtypemax);
					wcslcat(cmdLine, hWndVar->fileName, 2*wdirtypemax);
					wcslcat(cmdLine, L"\"", 2*wdirtypemax);
					STARTUPINFOW si;
					PROCESS_INFORMATION pi;
					SecureZeroMemory(&si, sizeof(si));
					si.cb = sizeof(si);
					si.wShowWindow = SW_SHOW;
					si.dwFlags = STARTF_USESHOWWINDOW;
					SecureZeroMemory(&pi, sizeof(pi));
					BOOL retVal = CreateProcessW(nullptr, cmdLine, nullptr, nullptr, FALSE, 0, nullptr, nullptr, &si, &pi);
					if (retVal)
					{
						hWndVar->dwProcessId = pi.dwProcessId;
						hWndVar->hProcess = pi.hProcess;
						DWORD dwWait = WaitForInputIdle(pi.hProcess, 8000);
						if (dwWait == 0)
						{
							Sleep(10);
							char className[_MAX_PATH];
							char wndName[_MAX_PATH];
							GetPrivateProfileStringA(WDXGUIDEINLISTER, "ClassName", 0, className, _MAX_PATH, iniFileName);
							GetPrivateProfileStringA(WDXGUIDEINLISTER, "WindowName", 0, wndName, _MAX_PATH, iniFileName);
							if (strlen(className) > 0 && strlen(wndName) > 0)
							{
								// hWndVar->hWndProc = FindWindowA("TForm1", "WDX Guide - testing of Content plugins");
								hWndVar->hWndProc = FindWindowA(className, wndName);
							}
							else
							{
								EnumWindows((WNDENUMPROC)EnumWindowsProc, (LPARAM)hWndVar);
							}
							if (hWndVar->hWndProc)
							{
								SetParent(hWndVar->hWndProc, hWnd);
								SetWindowLong(hWndVar->hWndProc, GWL_STYLE, WS_VISIBLE | WS_CLIPCHILDREN);
								RECT r;
								GetWindowRect(hWnd, &r);
								SetWindowPos(hWndVar->hWndProc, HWND_TOP, r.left, r.top, r.right, r.bottom, SWP_SHOWWINDOW);
							}
							else
							{
								bError = TRUE;
							}
						}
						else if (dwWait == WAIT_TIMEOUT)
						{
							MessageBoxW(hWnd, L"The wait was terminated because the time-out interval elapsed.", WDXGUIDEINLISTERW, MB_ICONSTOP | MB_OK);
							bError = TRUE;
						}
					}
					else
					{
						DisplayLastErrorMsgW(L"CreateProcessW", PathExW, MB_ICONSTOP | MB_OK);
						bError = TRUE;
					}
					delete[] cmdLine;

					if (bError)
					{
						PostMessageW(hWndVar->hWndParent, WM_CLOSE, 0, 0);
					}
				}
			}
			break;
		}

		case WM_PAINT:
		{
			WndVar* hWndVar = (WndVar*)GetWindowLongW(hWnd, GWLP_USERDATA);
			if (hWndVar && hWndVar->hWndProc)
			{
				RECT r;
				GetClientRect(hWnd, &r);
				RedrawWindow(hWndVar->hWndProc, &r, nullptr, RDW_INVALIDATE | RDW_ALLCHILDREN | RDW_ERASE);
				UpdateWindow(hWndVar->hWndProc);
			}
			break;
		}

		case WM_SHOWWINDOW:
		{
			WndVar* hWndVar = (WndVar*)GetWindowLongW(hWnd, GWLP_USERDATA);
			if (hWndVar && hWndVar->hWndProc)
			{
				if (wParam)
				{
					ShowWindow(hWndVar->hWndProc, SW_SHOW);
				}
				else
				{
					ShowWindow(hWndVar->hWndProc, SW_HIDE);
				}
				UpdateWindow(hWndVar->hWndProc);
			}
			break;
		}

		case WM_SIZE:
		{
			WndVar* hWndVar = (WndVar*)GetWindowLongW(hWnd, GWLP_USERDATA);
			if (hWndVar && hWndVar->hWndProc)
			{
				RECT r;
				GetClientRect(hWnd, &r);
				MoveWindow(hWndVar->hWndProc, r.left, r.top, r.right, r.bottom, TRUE);
			}
			break;
		}

		case WM_KEYDOWN:
		{
			switch (wParam)
			{
				case VK_ESCAPE:
				{
					WndVar* hWndVar = (WndVar*)GetWindowLongW(hWnd, GWLP_USERDATA);
					if (hWndVar && hWndVar->hWndParent)
					{
						DWORD dwStyle = GetWindowLongW(hWndVar->hWndParent, GWL_STYLE);
						if (dwStyle & WS_CHILD)
						{
							// quick view
						}
						else
						{
							// close lister window
							PostMessageW(hWndVar->hWndParent, WM_CLOSE, 0, 0);
						}
					}
					break;
				}

				default:
					return DefWindowProcW(hWnd, message, wParam, lParam);

			}
			break;
		}

		case WM_DESTROY:
		{
			WndVar* hWndVar = (WndVar*)GetWindowLongW(hWnd, GWLP_USERDATA);
			if (hWndVar && hWndVar->hWndProc)
			{
				PostMessageW(hWndVar->hWndProc, WM_CLOSE, 0, 0);
				hWndVar->hWndProc = nullptr;
				hWndVar->hProcess = nullptr;
			}
			break;
		}

		default:
		{
			// dirty hack to check if created process is still alive?
			WndVar* hWndVar = (WndVar*)GetWindowLongW(hWnd, GWLP_USERDATA);
			if (hWndVar)
			{
				DWORD dwExitCode;
				if (GetExitCodeProcess(hWndVar->hProcess, &dwExitCode))
				{
					if (dwExitCode != STILL_ACTIVE)
					{
						hWndVar->hWndProc = nullptr;
						hWndVar->hProcess = nullptr;
						PostMessageW(hWndVar->hWndParent, WM_CLOSE, 0, 0);
					}
				}
			}
			return DefWindowProcW(hWnd, message, wParam, lParam);
		}
	}
	return 0;
}

void __stdcall ListGetDetectString(char* DetectString, int maxlen)
{
	if (DetectString)
	{
		strlcpy(DetectString, _detectstring, maxlen);
	}
}

void __stdcall ListSetDefaultParams(ListDefaultParamStruct* dps)
{
	if (dps)
	{
		strlcpy(iniFileName, dps->DefaultIniName, _MAX_PATH);
		char Path[_MAX_PATH];
		GetPrivateProfileStringA(WDXGUIDEINLISTER, PATH, 0, Path, _MAX_PATH, iniFileName);
		ExpandEnvironmentStringsA(Path, PathEx, _MAX_PATH);
	}
}

HWND __stdcall ListLoad(HWND ParentWin, char* FileToLoad, int ShowFlags)
{
	wchar_t FileToLoadW[wdirtypemax];
	return ListLoadW(ParentWin, awfilenamecopy(FileToLoadW, FileToLoad), ShowFlags);
}

HWND __stdcall ListLoadW(HWND ParentWin, wchar_t* FileToLoad, int ShowFlags)
{
	HWND hWnd = nullptr;
	WNDCLASSEXW WinClass;
	SecureZeroMemory(&WinClass, sizeof(WNDCLASSEXW));
	WinClass.style         = CS_HREDRAW | CS_VREDRAW;
	WinClass.lpfnWndProc   = static_cast<WNDPROC>(WndProc);
	WinClass.cbClsExtra    = 0;
	WinClass.cbWndExtra    = DLGWINDOWEXTRA;
	WinClass.hInstance     = hinst;
	WinClass.hIcon         = LoadIcon(nullptr, IDI_APPLICATION);
//	WinClass.hbrBackground = (HBRUSH)(COLOR_BTNFACE + 1);
	WinClass.hCursor       = LoadCursor(nullptr, IDC_ARROW);
	WinClass.lpszClassName = WDXGUIDEINLISTERW;
	WinClass.cbSize        = sizeof(WNDCLASSEX);
	WinClass.hIconSm       = LoadIcon(nullptr, IDI_APPLICATION);

	RegisterClassExW(&WinClass);

	WndVar* hWndVar = new WndVar;
	if (hWndVar)
	{
		SecureZeroMemory(hWndVar, sizeof(WndVar));
		wcslcpy(hWndVar->fileName, FileToLoad, wdirtypemax);
		hWndVar->hWndParent = ParentWin;
		RECT r;
		GetClientRect(ParentWin, &r);
		hWnd = CreateWindowExW(0, WDXGUIDEINLISTERW, L"", WS_VISIBLE | WS_CHILD, r.left, r.top, r.right - r.left, r.bottom - r.top, ParentWin, nullptr, hinst, (LPVOID)hWndVar);
	}
	return hWnd;
}

void __stdcall ListCloseWindow(HWND ListWin)
{
	if (ListWin)
	{
		WndVar* hWndVar = (WndVar*)GetWindowLongW(ListWin, GWLP_USERDATA);
		DestroyWindow(ListWin);
		if (hWndVar)
		{
			delete hWndVar;
		}
	}
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved)
{
	switch (fdwReason)
	{
		case DLL_PROCESS_ATTACH :
			hinst = hinstDLL;
			break;

		case DLL_PROCESS_DETACH :
			hinst = nullptr;
			break;
	}
	return TRUE;
}
