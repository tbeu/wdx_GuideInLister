﻿WDXGuideInLister 1.0 plugin for Total Commander

EN
* Description
WDX Guide is a nice tool for testing WDX plugins for Total Commander.
I wanted to use this utility as a Lister plugin, so I wrote WDXGuideInLister.
he purpose of this plugin is to launch WDX Guide inside the Lister window.
May be someone will find this plugin useful too. 

* Requirements
Total Commander 7.57a and higher 
WDX Guide 

RU
* Описание
WDX Guide это утилита для тестирования WDX плагинов для Total Commander.
Мне хотелось пользоваться этой утилитой так, как будто это листер плагин, 
поэтому я написал плагин WDXGuideInLister. Цель данного плагина показывать 
окно WDX Guide внутри Листера. Возможно, это пригодится еще кому-нибудь. 

* Требования
Total Commander 7.57a или выше 
Установленный WDX Guide


© 2005-2014 Dmitrie Murzaikin, Thomas Beutlich